import _ from 'lodash'
import './scss/app.scss'
import './css/tailwindcss.css'

function createElement() {
  const el = document.createElement('div');

  el.innerHTML = _.join(['Une', 'chope', '?'], ' ')
  return el
}

document.body.appendChild(createElement())
