const path = require('path')
const htmlWebpackPlugin = require('html-webpack-plugin')
const miniCssExtractPlugin = require('mini-css-extract-plugin')
const glob = require('glob')

let config = {
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: ['babel-loader']
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          miniCssExtractPlugin.loader,
          // Translates CSS into CommonJS
          'css-loader',
          // Compiles Sass to CSS
          'sass-loader',
        ],
      },
      {
        test: /\.ejs$/,
        use: ['ejs-loader']
      }
    ]
  },
  plugins: [
    new miniCssExtractPlugin({
      filename: 'style.css'
    }),
  ]
}

//get all files with .ejs extension
// process.cwd() she will look the current directory with the command in terminal
// autrement dit, ça ira directement à la racinne du projet
const files = glob.sync(process.cwd() + '/src/views/*.ejs')

//boucle sur tous les fichiers réccupérés
//ensuite sur chaque fichiers on va lancer la config webpack
//pour que le plugin htmlWebpackPlugin puisse bosser sur chaque fichier
//et ainsi sortir un html correct
files.forEach(file => {
  config.plugins.push(new htmlWebpackPlugin({
    filename: path.basename(file).replace('.ejs', '.html'),
    template: file
  }))
})
module.exports = config
